using System;
using System.Collections;
using UnityEditor;
using UnityEngine;

public class IsVisible : MonoBehaviour
{
    public Renderer objectRenderer;
    public Camera mainCamera;
    private void Start()
    {
        if (IsVisibleInMainCamera())
        {
            StartCoroutine(CameraPositionFix());
        }
        else
        {
            StartCoroutine(CameraPositionSuperFix());
        }
    }

    bool IsVisibleInMainCamera()
    {
        Plane[] planes = GeometryUtility.CalculateFrustumPlanes(mainCamera);
        return GeometryUtility.TestPlanesAABB(planes, objectRenderer.bounds);
    }
    

    private IEnumerator CameraPositionFix()
    {
        while (IsVisibleInMainCamera())
        {
            Vector3 pos = mainCamera.transform.position;
            pos.y += 0.01f;
            pos.z += 0.01f;
            mainCamera.transform.position = pos;
            yield return null;
        }
        if(Screen.height/Screen.width != 1920/1080)
            PrecisionFix(-0.04f);
    }
    
    private IEnumerator CameraPositionSuperFix()
    {
        while (!IsVisibleInMainCamera())
        {
            Vector3 pos = mainCamera.transform.position;
            pos.y -= 0.01f;
            pos.z -= 0.01f;
            mainCamera.transform.position = pos;
            yield return null;
        }
        if(Screen.height/Screen.width != 1920/1080)
            PrecisionFix(0.04f);
    }

    private void PrecisionFix(float offset)
    {
        Vector3 pos1 = mainCamera.transform.position;
        pos1.y += offset;
        pos1.z += offset;
        mainCamera.transform.position = pos1;
    }
}