using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;
public class GameManager : MonoBehaviour
{
        
    [Serializable]
    public class WeightedItem
    {
        public GameObject item;
        public float weight;
    }
    
    public List<WeightedItem> weightedItems;
        
    public Transform parentObject;
    public int numberOfBubble;
    public Vector3 minPosition;
    public Vector3 maxPosition;
    public float minYDistance = 1f;
    public List<GameObject> gameObjectsOnScreen;
    public Camera mainCamera;

    private float yMinPosition, zMinPosition;
    
    private void Start()
    {
        Application.targetFrameRate = 60;
        // CheckResolution();
        
        yMinPosition = minPosition.y;
        zMinPosition = minPosition.z;
        CreateBubbles();
    }
    
    void Update()
    {
        if (!Input.GetMouseButtonDown(0)) return;
        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit[] hits = Physics.RaycastAll(ray);
        Transform firstBubble = null;

        foreach (var hit in hits)
        {
            if (firstBubble is null && "BubbleLayer" == LayerMask.LayerToName(hit.collider.gameObject.layer))
            {
                firstBubble = hit.collider.transform;
                break;
            }
        }
        foreach (var hit in hits)
        {
            if ("FishLayer" == LayerMask.LayerToName(hit.collider.gameObject.layer))
            {
                if(IsChildOf(hit.collider.gameObject, firstBubble))
                {
                    hit.collider.gameObject.GetComponent<Fish>().DestroyFish();
                    break;
                }
            }
        }
            
        /*foreach (RaycastHit hit in hits)
        {
            if ((comparedLayer.value & 1 << hit.collider.gameObject.layer) != 0)
            {
                hit.collider.gameObject.GetComponent<Fish>().DestroyFish();
                break;
            }
        }*/
    }
    
    
    bool IsChildOf(GameObject child, Transform parent)
    {
        if (child.transform.parent == null)
            return false;

        if (child.transform.parent == parent)
            return true;

        return IsChildOf(child.transform.parent.gameObject, parent);
    }

    
    public void Generate()
    {
        minPosition.y = yMinPosition;
        minPosition.z = zMinPosition;
        Reset();
    }
    
    private void CreateBubbles()
    {
        for (int i = 0; i < numberOfBubble; i++)
        {
            Vector3 position = new Vector3(
                Random.Range(minPosition.x, maxPosition.x),
                minPosition.y,
                minPosition.z
            );
            GameObject go = Instantiate(PickRandomObject(), position, Quaternion.identity, parentObject);
            go.transform.rotation = Quaternion.identity;
            gameObjectsOnScreen.Add(go);
            minPosition.y += minYDistance;
            minPosition.z += minYDistance;
        }
    }
        
    public GameObject PickRandomObject()
    {
        float totalInvertedWeight = 0;
        foreach (WeightedItem item in weightedItems)
        {
            totalInvertedWeight += 1 / item.weight;
        }
    
        float randomValue = Random.Range(0f, totalInvertedWeight);
    
        float weightSum = 0;
        foreach (WeightedItem item in weightedItems)
        {
            weightSum += 1 / item.weight;
            if (randomValue <= weightSum)
            {
                return item.item;
            }
        }
        return null;
    }
    
    public void Reset()
    {
        foreach (var t in gameObjectsOnScreen)
        {
            Destroy(t);
        }
        gameObjectsOnScreen.Clear();
        CreateBubbles();
    }

    private void CheckResolution()
    {
        float screenHeight = Screen.height;
        float screenWidth = Screen.width;
        float ratio = screenHeight / screenWidth;
        // Debug.Log(ratio);
        float orthographicSize = 2.8f;
        float yPos = -6.28f;

        switch (ratio)
        {
            case >= 2.4f:
                orthographicSize = 3.8f;
                yPos = -5.4f;
                break;
            case >= 2.164f:
                orthographicSize = 3.4f;
                yPos = -5.7f;
                break;
            case >= 2.056f:
                orthographicSize = 3.25f;
                yPos = -5.9f;
                break;
            case >= 2f:
                orthographicSize = 3.15f;
                yPos = -5.95f;
                break;
            case >= 1.7f:
                orthographicSize = 2.8f;
                yPos = -6.28f;
                break;
            case >= 1.6f:
                orthographicSize = 2.6f;
                yPos = -6.45f;
                break;
            case >= 1.33f:
                orthographicSize = 2.1f;
                yPos = -6.9f;
                break;
        }

        mainCamera.orthographicSize = orthographicSize;
        Vector3 position = mainCamera.transform.position;
        position.y = yPos;
        mainCamera.transform.position = position;
    }
}