using UnityEngine;

public class IgnoreParentScale : MonoBehaviour
{
    private Vector3 originalScale; // Store the original scale of the child object

    void Start()
    {
        // Store the original scale of the child object
        originalScale = transform.localScale;

        // Set the local scale of the child object to be relative to the world scale
        transform.localScale = Vector3.one;
    }

    void FixedUpdate()
    {
        // Apply the original scale to compensate for the parent's scale
        transform.localScale = new Vector3(
            originalScale.x / transform.parent.localScale.x,
            originalScale.y / transform.parent.localScale.y,
            originalScale.z / transform.parent.localScale.z
        );
    }
}