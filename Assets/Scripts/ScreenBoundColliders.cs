using UnityEngine;

public class ScreenBoundColliders : MonoBehaviour
{
    private Camera mainCamera;
    public Transform left, right;

    private void Start()
    {
        // Get the main camera
        mainCamera = Camera.main;

        // Call the method to find and print the viewport sides
        FindAndPrintViewportSides();
    }

    private void FindAndPrintViewportSides()
    {
        // Calculate the viewport corners in world space
        Vector3 bottomLeft = mainCamera.ViewportToWorldPoint(new Vector3(0, 0, mainCamera.nearClipPlane));
        bottomLeft.x += 0.2f;
        // Vector3 topLeft = mainCamera.ViewportToWorldPoint(new Vector3(0, 1, mainCamera.nearClipPlane));
        // Vector3 bottomRight = mainCamera.ViewportToWorldPoint(new Vector3(1, 0, mainCamera.nearClipPlane));
        // Vector3 topRight = mainCamera.ViewportToWorldPoint(new Vector3(1, 1, mainCamera.nearClipPlane));

        // Print the positions of the viewport corners
        left.transform.position = new Vector3(bottomLeft.x, left.transform.position.y, left.transform.position.z);
        right.transform.position = new Vector3(-bottomLeft.x, right.transform.position.y, right.transform.position.z);
    }
}