using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fish : MonoBehaviour
{
    public Bubble bubble;
    public void DestroyFish()
    {
        bubble.fishes.Remove(gameObject);
        Destroy(gameObject);
        if (bubble.fishes.Count == 0) Destroy(bubble.gameObject, 0.2f);
    }

    /*public void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(Camera.main.transform.position, transform.position);
    }*/
}
